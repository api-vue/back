import Security from "../security.js";
import {ChatGPTAPI} from 'chatgpt'
import articles from "../database/article.json" assert {type: "json"};
import ArticleRepository from "../Repository/ArticleRepository.js";
import * as fs from "fs";
import Article from "../Entity/Article.js";
import moment from "moment";
import {watch} from "node:fs"

export default class ArticleApi {
    security
    pagination = 10
    page = 1

    constructor(app) {
        this._app = app;
        this.security = new Security(app)
        this.launchRoutes()
    }

    launchRoutes () {
        this.download();
        this.get();
        this.getOne();
        this.post();
        this.generateArticle();
        this.delete();
        this.put();
    }

    get() {
        this._app.get("/api/articles", (req, res) => {
            this.pagination = req.query['pagination'] ?? this.pagination
            this.page = req.query['page'] ?? this.page

            let retArticles = articles;

            if (req.query) {
                if (!('isAdminPage' in req.query &&  eval(req.query['isAdminPage']))) {
                    retArticles = retArticles.filter(ArticleRepository.findOnlyPublished())
                }
                if ('title' in req.query) {
                    retArticles = retArticles.filter(ArticleRepository.findByTitle(req.query['title']))
                }
                if ('author' in req.query) {
                    retArticles = retArticles.filter(ArticleRepository.findByAuthor(req.query['author']))
                }
                if ('search' in req.query) {
                    retArticles = retArticles.filter(ArticleRepository.findBySearch(req.query['search']))
                }
            }
            let total = retArticles.length;

            retArticles = retArticles.slice((this.page - 1) * this.pagination, this.page * this.pagination);

            let result = {
                'results': retArticles,
                'total': total,
                'totalPage': Math.ceil(total / this.pagination),
                'page': this.page
            };

            return res.status(200).json({
                message: result
            })
        });
    }

    getOne() {
        this._app.get("/api/articles/:id", (req, res) => {
            const params = req.params;

            if (isNaN(req.params.id)) {
                return ;
            }

            let article = articles.find(ArticleRepository.findById(params.id));

            if (article.isPublished === false) {
                const user = this.security.isLoggedIn(req, res);
                if (user === false) return res;
            }

            if (!article) {
                return res.status(404).json({
                    message: "This article cannot be found"
                })
            }

            return res.status(200).json({
                message: article
            })
        });
    }

     generateArticle() {
        this._app.post("/api/articles/generate", async (req, res) => {
            const user = this.security.isLoggedIn(req, res);
            if (user === false) return res;

            const title = req.body.title;

            const api = new ChatGPTAPI({
                apiKey: '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'
            })

            const resGpt = await api.sendMessage('Ecris moi un article avec des paragraphes sur le thème suivant :' + title);

            return res.status(201).json({
                message: resGpt.text,
            })
        });
    }

    delete() {
        this._app.delete('/api/articles/:id', (req, res) => {
            const user = this.security.isLoggedIn(req, res);
            if (user === false) return res;

            if (user.role !== 'admin'){
                return res.status(401).json({message: "You don't have the right to delete this"})
            }

            let articleId = articles.findIndex(ArticleRepository.findById(req.params.id));
            if (articleId === undefined || articleId < 0) return res.status(404).json({message: 'Article not found'})

            articles.splice(articleId, 1);

            fs.writeFile ("./database/article.json", JSON.stringify(articles), function(err) {
                    if (err) throw err;
                    console.log('complete');
                }
            );

            return res.status(201).json();
        })
    }

    post() {
        this._app.post('/api/articles', (req, res) => {
            const user = this.security.isLoggedIn(req, res);
            if (user === false) return res;

            if (user.role !== 'admin') {
                return res.status(401).json({message: "You don't have the right to post this"})
            }

            if (!req.body.title || !req.body.content) {
                return res.status(400).json({
                    "message": "Please provide a valid body"
                })
            }

            let article = new Article(req.body.title, req.body.content);
            article.author = user.username;

            if (req.body.isPublished) {
                article.isPublished = req.body.isPublished
            }

            articles.push(article);

            fs.writeFile('./database/article.json', JSON.stringify(articles), function(err) {
                    if (err) throw err;
                    console.log('complete');
                }
            );

            return res.status(201).json();
        })
    }

    put() {
        this._app.put('/api/articles/:id', (req, res) => {
            const user = this.security.isLoggedIn(req, res);
            if (user === false) return res;

            if (user.role !== 'admin'){
                return res.status(401).json({message: "You don't have the right to update this"})
            }

            let articleId = articles.findIndex(ArticleRepository.findById(req.params.id));
            if (articleId === undefined || articleId < 0) return res.status(404).json({message: 'Article not found'})

            let article = articles[articleId];

            for (const bodyKey in req.body) {
                if (article[bodyKey] !== undefined) {
                    article[bodyKey] = req.body[bodyKey]
                }
            }

            articles[articleId] = article

            fs.writeFile ("./database/article.json", JSON.stringify(articles), function(err) {
                    if (err) throw err;
                    console.log('complete');
                }
            );

            return res.status(201).json();
        })
    }

    download() {
        this._app.get('/api/articles/download', async (req, res) => {
            let fileName = './database/article.json';
            if (req.query['type'] && req.query['type'] === 'csv') {
                let retArticles = articles;
                retArticles = ArticleRepository.convertToCsv(retArticles);
                let date = moment();
                fileName = './export/export_article_' + date.format('YYYY-MM-DD_HH-mm-ss') + '.csv';
                fs.appendFile(fileName, retArticles, function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                });
                await this.checkFileExist(fileName);
            }

            return res.status(200).download(fileName);
        })
    }

    async checkFileExist(path, timeout = 1000) {
        let totalTime = 0;
        let checkTime = timeout / 10;
        return await new Promise((resolve, reject) => {
            const timer = setInterval(function() {
                totalTime += checkTime;
                let fileExists = fs.existsSync(path);
                if (fileExists || totalTime >= timeout) {
                    clearInterval(timer);
                    resolve(fileExists);
                }
            }, checkTime);
        });

    }
}
