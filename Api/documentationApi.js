import swaggerUi from "swagger-ui-express";
import swaggerDocument from "../swagger.json" assert {type: "json"};

export default class DocumentationApi {
    constructor(app) {
        this._app = app
        this.launchRoutes()
    }

    launchRoutes() {
        this.swaggerUi();
        this.swaggerJson()
    }

    swaggerUi() {
        this._app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    }

    swaggerJson() {
        this._app.get('/api-docs-json', (req, res) => {
            return res.status(201).json(swaggerDocument)
        })
    }
}