import articles from '../database/article.json' assert {type: "json"};

export default class Article {
    id
    author
    isPublished = false
    constructor(title, content)
    {
        this.title = title;
        this.content = content;
        this.id = articles[articles.length - 1].id + 1;
    }
}