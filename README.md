# API Documentation
=================

This API provides endpoints to manage articles, including retrieving, creating, updating, and deleting articles. It also has an endpoint to generate article content based on a given title using ChatGPT.

## Group

- Arthur Brunot
- Eliot Lauger
- Gabriel Quesnot
- Louis Defaut
- Lucas Montant

## Endpoints
---------

### `GET /api/articles/:id`

Retrieves an article by its ID.

#### Parameters

-   `id`: the ID of the article to retrieve

#### Response

-   Status Code: `200`
-   Body: the article object

### `GET /api/articles`

Retrieves a list of all articles.

#### Response

-   Status Code: `200`
-   Body: an array of article objects

### `POST /api/articles`

Creates a new article.

#### Request Body

-   `title`: the title of the article (required)
-   `content`: the content of the article (required)

#### Response

-   Status Code: `201`
-   Body: the created article object

### `POST /api/articles/generate`

Generates content for a new article based on a given title using ChatGPT.

#### Request Body

-   `title`: the title of the article (required)

#### Response

-   Status Code: `201`
-   Body: the created article object with generated content

### `PUT /api/articles/:id`

Updates an article by its ID.

#### Parameters

-   `id`: the ID of the article to update

#### Request Body

-   `title`: the new title of the article (optional)
-   `content`: the new content of the article (optional)

#### Response

-   Status Code: `200`
-   Body: the updated article object

### `DELETE /api/articles/:id`

Deletes an article by its ID.

#### Parameters

-   `id`: the ID of the article to delete

#### Response

-   Status Code: `204`

## Export Endpoints
----------------

### `GET /api/articles/export`

Export the database to json

### `GET /api/articles/export/?type=csv`

Export the database to csv

#### Response

-   Status Code: `200`

## API Documentation Endpoints
---------------------------

### `GET /api-docs`

Displays Swagger UI to interact with the API and view its documentation.

### `GET /api-docs-json`

Returns the OpenAPI specification for the API in JSON format.

## Technologies Used
-----------------

-   Node.js
-   Express
-   ChatGPT

## Getting Started
---------------

1.  Clone this repository
2.  Install dependencies using `npm install`
3.  Set up environment variables for your database connection and ChatGPT credentials
4.  Start the server using `node index.js` or `nodemon`

## Contributing
------------

If you would like to contribute to this project, please fork the repository and submit a pull request.
