export default class ArticleRepository {
    static findById(id) {
        return (item) => item.id.toString() === id.toString();
    }

    static findByTitle(title) {
        return (item) => item.title.toString() === title.toString();
    }

    static findByAuthor(author) {
        return (item) => item.author.toString() === author.toString();
    }

    static findOnlyPublished() {
        return (item) => item.isPublished === true
    }

    static findBySearch(search) {
        return (item) => (
            item.author.includes(search.toString()) ||
            item.title.includes(search.toString())
        )
    }

    static convertToCsv(arr) {
        const keys = Object.keys(arr[0]);
        const replacer = (_key, value) => value === null ? '' : value;
        const processRow = row => keys.map(key => JSON.stringify(row[key], replacer)).join(',');
        return [ keys.join(','), ...arr.map(processRow) ].join('\r\n');
    };
}