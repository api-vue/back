import express from "express";
import Security from "./security.js";
import ArticleApi from "./Api/articleApi.js";
import DocumentationApi from "./Api/documentationApi.js";
import cors from "cors";

const app = express();
app.use(express.json());
app.use(cors());

const security = new Security(app);
new ArticleApi(app);
new DocumentationApi(app);

security.login();

app.listen(process.env.PORT || 8080);
