import jwt from "jsonwebtoken";
import users from './database/user.json' assert { type: 'json' };
import User from "./Entity/User.js";
import ArticleRepository from "./Repository/ArticleRepository.js";
const JWT_SECRET = "goK!pusp6ThEdURUtRenOwUhAsWUCLheBazl!uJLPlS8EbreWLdrupIwabRAsiBu";

export default class Security {
    app;

    constructor(app) {
        this.app = app;
    }

    login() {
        this.app.post("/api/login", (req, res) => {
            const {username, password} = req.body;
            console.log(`${username} is trying to login ..`);

            for (const user of users) {
                if (username === user.username && password === user.password) {
                    return res.json({
                        token: jwt.sign({user: user.username}, JWT_SECRET),
                    });
                }
            }
            return res
                .status(401)
                .json({message: "The username and password your provided are invalid"});
        });
    }

    isLoggedIn (req, res) {
        if (!req.headers.authorization) {
            res.status(401).json({error: "Not Authorized"});
            return false;
        }
        const authHeader = req.headers.authorization;
        const token = authHeader.split(" ")[1];

        try {
            // const { user } = jwt.verify(token, JWT_SECRET);
            const { user } = jwt.verify(token, JWT_SECRET);
            return users.find((usr) => usr.username === user)
        } catch (error) {
            res.status(401).json({error: "Not Authorized"});
            return false;
        }
    }
}
